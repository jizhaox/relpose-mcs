## 1. Overview

This code is an implementation of the following paper. The core solvers are written by C++. Matlab Mex interfaces and demo codes are also provided.

## 2. Quick Start

2.1 run compile.m in subfolders of "solvers" to compile the mex files. 

Make sure to set the proper path of Eigen library in compile.m. This step can be skipped once the mex files have been compiled. Compiled files on Ubuntu 16.04 and Matlab R2019a are provided.

2.2 run test_solver*.m in folder "test"
