clear;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('--------------2AC method & case1~4--------------')
%% generate synthetic data
match_info = 'case1';
[Image1, Image2, At, R_cam, t_cam, R_gt, cay_gt, t_gt] = generate_2AC_data(match_info);
[R_cam2, t_cam2] = generic_interface_ac(R_cam, t_cam, match_info);

%% run solver
% set the 6th parameter as 0 (default), 48 solutions
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_generic_2ac(Image1, Image2, At, R_cam2, t_cam2, 0);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

% set the 6th parameter as 1, 64 solutions.
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_generic_2ac(Image1, Image2, At, R_cam2, t_cam2, 1);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('--------------2AC method & case5--------------')
%% generate synthetic data
[Image1, Image2, At, R_cam, t_cam, R_gt, cay_gt, t_gt] = generate_2AC_data( 'case5');

%% run solver
% set the 6th parameter as 0 (default), 48 solutions
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_case5_2ac(Image1, Image2, At, R_cam, t_cam, 0);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

% set the 6th parameter as 1, 64 solutions.
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_case5_2ac(Image1, Image2, At, R_cam, t_cam, 1);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('--------------2AC method & inter-camera case--------------')
%% generate synthetic data
[Image1, Image2, At, R_cam, t_cam, R_gt, cay_gt, t_gt] = generate_2AC_data('inter');
    
%% run solver
% set the 6th parameter as 0 (default), 48 solutions
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_inter_2ac(Image1, Image2, At, R_cam, t_cam, 0);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

% set the 6th parameter as 1, 56 solutions.
% according to our evaluation, 56-solution configuration has better numerical stability
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_inter_2ac(Image1, Image2, At, R_cam, t_cam, 1);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('--------------2AC method & intra-camera case--------------')
[Image1, Image2, At, R_cam, t_cam, R_gt, cay_gt, t_gt] = generate_2AC_data('intra');

%% run solver
% 48 solutions
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_intra_2ac(Image1, Image2, At, R_cam, t_cam);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

% add coordinate transformation
% according to our evaluation, it has better numerical stability for some cases
[cay_sols, t_sols, R_sols] = solver_trans_intra_2ac_enhance(Image1, Image2, At, R_cam, t_cam);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt


