function [match_info, n_cam] = match_type_gcam_ac(match_type)

if nargin<1
    match_type = 'case1';
end

%% Two types of affine correspondence
%% inter-cam affine correspondences contain
% (1) 1 affine correspondence: (cam1 at time i) <--> (cam2 at time j)
% (2) 1 affine correspondence: (cam2 at time i) <--> (cam1 at time j)
%% intra-cam affine correspondences contain
% (1) 1 affine correspondence: (cam1 at time i) <--> (cam1 at time j)
% (2) 1 affine correspondence: (cam2 at time i) <--> (cam2 at time j)

if strcmp(match_type, 'case1')
    % AC of 4 cameras
    n_cam = 4;
    match_info{1} = struct('idx1', 1, 'idx2', 2);
    match_info{2} = struct('idx1', 3, 'idx2', 4);
elseif strcmp(match_type, 'case2')
    % AC of 3 cameras
    n_cam = 3;
    match_info{1} = struct('idx1', 1, 'idx2', 2);
    match_info{2} = struct('idx1', 2, 'idx2', 3);
elseif strcmp(match_type, 'case3')
    % AC of 3 cameras
    n_cam = 3;
    match_info{1} = struct('idx1', 1, 'idx2', 1);
    match_info{2} = struct('idx1', 2, 'idx2', 3);
elseif strcmp(match_type, 'case4')
    % AC of 3 cameras
    n_cam = 3;
    match_info{1} = struct('idx1', 1, 'idx2', 3);
    match_info{2} = struct('idx1', 2, 'idx2', 3);
elseif strcmp(match_type, 'case5')
    % AC of 2 cameras
    n_cam = 2;
    match_info{1} = struct('idx1', 1, 'idx2', 2);
    match_info{2} = struct('idx1', 2, 'idx2', 2);
elseif strcmp(match_type, 'inter') || strcmp(match_type, 'case6')
    % AC of 2 cameras
    n_cam = 2;
    match_info{1} = struct('idx1', 1, 'idx2', 2);
    match_info{2} = struct('idx1', 2, 'idx2', 1);
elseif strcmp(match_type, 'intra') || strcmp(match_type, 'case7')
    % AC of 2 cameras
    n_cam = 2;
    match_info{1} = struct('idx1', 1, 'idx2', 1);
    match_info{2} = struct('idx1', 2, 'idx2', 2);
else
    error('match type error!')
end

