function [Image1, Image2, R_extrinsic_para, T_extrinsic_para, R_gt, cay_gt, t_gt] = generate_6pc_data(match_type)
% Author: Ji Zhao
% Email: zhaoji84@gmail.com
% 01/27/2021
% Reference:
% [1] 


%% Two types of correspondence
%% inter-cam correspondences contain
% (1) 3 correspondences: (cam1 at time i) <--> (cam2 at time j)
% (2) 3 correspondences: (cam2 at time i) <--> (cam1 at time j)
%% intra-cam correspondences contain
% (1) 3 correspondences: (cam1 at time i) <--> (cam1 at time j)
% (2) 3 correspondences: (cam2 at time i) <--> (cam2 at time j)

n_cam = 2;
n_point = 6;

%% define random extrinsic parameters
cam_body_rotation = cell(n_cam, 1);
cam_body_offset = cell(n_cam, 1);
R_cam = cell(n_cam, 1);
t_cam = cell(n_cam, 1);
T_body_cam = cell(n_cam, 1);
for ii = 1:n_cam
    cay = rand(3, 1);
    cam_body_rotation{ii} = cayley_rotation(cay);
    cam_body_offset{ii} = rand(3, 1);
    
    R_cam{ii} = cam_body_rotation{ii}';
    t_cam{ii} = -R_cam{ii}*cam_body_offset{ii};
    % transformation from body reference to perspective camera references
    T_body_cam{ii} = [R_cam{ii} t_cam{ii}; 0 0 0 1];
end

%% define relative pose
cay = rand(3, 1);
R_gt = cayley_rotation(cay);
q = rotm2quat(R_gt);
cay_gt = q(2:4)/q(1);
cay_gt = cay_gt(:);

t_gt = rand(3, 1);

% transformation from body reference at time i to time j
T_gt = [R_gt t_gt; 0 0 0 1];

%% generating random scene points
points_all = cell(n_point, 1);
for ii = 1:n_point
    PT = rand(3, 1);
    points_all{ii} = struct('point', PT);
end

%% Part 2: extract point observations
% images at time i
x_i = cell(n_point, n_cam);
for ii = 1:n_point
    PT = points_all{ii}.point;
    for jj = 1:n_cam
        tmp = R_cam{jj}*PT+t_cam{jj};
        x_i{ii,jj} = tmp/norm(tmp);
    end
end
% images at time j
Rc_j = cell(n_cam, 1);
tc_j = cell(n_cam, 1);
for ii = 1:n_cam
    tmp = T_body_cam{ii}*T_gt;
    Rc_j{ii} = tmp(1:3,1:3);
    tc_j{ii} = tmp(1:3,4);
end
x_j = cell(n_point, n_cam);
for ii = 1:n_point
    PT = points_all{ii}.point;
    for jj = 1:n_cam
        tmp = Rc_j{jj}*PT+tc_j{jj};
        x_j{ii,jj} = tmp/norm(tmp);
    end
end

%% construct observations
R_extrinsic_para = cat(3, cam_body_rotation{1}, cam_body_rotation{2});
T_extrinsic_para = [cam_body_offset{1}, cam_body_offset{2}];
Image1 = zeros(3, 6);
Image2 = zeros(3, 6);
if strcmp(match_type, 'inter')
    for ii = 1:3
        Image1(:,ii) = x_i{ii,1};
        Image2(:,ii) = x_j{ii,2};
    end
    for ii = 4:6
        Image1(:,ii) = x_i{ii,2};
        Image2(:,ii) = x_j{ii,1};
    end
elseif strcmp(match_type, 'intra')
    for ii = 1:3
        Image1(:,ii) = x_i{ii,1};
        Image2(:,ii) = x_j{ii,1};
    end
    for ii = 4:6
        Image1(:,ii) = x_i{ii,2};
        Image2(:,ii) = x_j{ii,2};
    end
else
    error('unsupported match type!')
end





