clear;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('--------------6-point method & inter-camera case--------------')
%% generate synthetic data
[Image1, Image2, R_cam, t_cam, R_gt, cay_gt, t_gt] = generate_6pc_data('inter');

%% check data
% For noise-free data, the residual of epipolar geometry equation should be small
err_epipolar = zeros(6, 1);
Tf_gt = [R_gt t_gt; 0 0 0 1];
n_cam = size(R_cam,3);
Tf_cam = cell(n_cam, 1);
for ii = 1:n_cam
    Tf_cam{ii} = [R_cam(:,:,ii), t_cam(:,ii); 0, 0, 0, 1];
end
for ii = 1:6
    if ii <= 3
        idx1 = 1;
        idx2 = 2;
    else
        idx1 = 2;
        idx2 = 1;
    end
    x1 = Image1(:, ii);
    x2 = Image2(:, ii);
    % relative pose between time i and time j 
    Hij = Tf_cam{idx2}\Tf_gt*(Tf_cam{idx1});
    Rij = Hij(1:3,1:3);
    tij = Hij(1:3,4);
    % essential matrix
    x = tij;
    skew_tij=[0 -x(3) x(2) ; x(3) 0 -x(1) ; -x(2) x(1) 0 ];
    E =skew_tij*Rij;
    err_epipolar(ii) = x2'*E*x1;
end
disp(['The maximum residual of epipolar geometry: ' num2str(max(abs(err_epipolar(:))))]);
    
%% run solver
% set the 5th parameter as 0 (default), 48 solutions
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_inter_6pc(Image1, Image2, R_cam, t_cam, 0);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

% set the 5th parameter as 1, 56 solutions.
% according to our evaluation, 56-solution configuration has better numerical stability
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_inter_6pc(Image1, Image2, R_cam, t_cam, 1);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('--------------6-point method & intra-camera case--------------')
[Image1, Image2, R_cam, t_cam, R_gt, cay_gt, t_gt] = generate_6pc_data('intra');

%% check data
% For noise-free data, the residual of epipolar geometry equation should be small
err_epipolar = zeros(6, 1);
Tf_gt = [R_gt t_gt; 0 0 0 1];
n_cam = size(R_cam,3);
Tf_cam = cell(n_cam, 1);
for ii = 1:n_cam
    Tf_cam{ii} = [R_cam(:,:,ii), t_cam(:,ii); 0, 0, 0, 1];
end
for ii = 1:6
    if ii <= 3
        idx1 = 1;
        idx2 = 1;
    else
        idx1 = 2;
        idx2 = 2;
    end
    x1 = Image1(:, ii);
    x2 = Image2(:, ii);
    % relative pose between time i and time j 
    Hij = Tf_cam{idx2}\Tf_gt*(Tf_cam{idx1});
    Rij = Hij(1:3,1:3);
    tij = Hij(1:3,4);
    % essential matrix
    x = tij;
    skew_tij=[0 -x(3) x(2) ; x(3) 0 -x(1) ; -x(2) x(1) 0 ];
    E =skew_tij*Rij;
    err_epipolar(ii) = x2'*E*x1;
end
disp(['The maximum residual of epipolar geometry: ' num2str(max(abs(err_epipolar(:))))]);

%% run solver
% 48 solutions
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_intra_6pc(Image1, Image2, R_cam, t_cam);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt

%% use reference transformation to improve numerical stability
[cay_sols, t_sols, R_sols] = solver_trans_intra_6pc_enhance(Image1, Image2, R_cam, t_cam);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt


