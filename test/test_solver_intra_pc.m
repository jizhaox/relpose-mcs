%% test solver for intra-camera PCs

%% Interface of the solver
% [q_sols, t_sols, R_sols, q_sols_all, A] = solver_trans_intra_6pc(Image1, Image2, R_camera, T_camera);
% Image1: homogeneous image coordinates (aka 3D bearing vectors) at view 1
%   size is 3*6
%   column 1-3: PCs that appears in camera 1 of view 1
%   column 4-6: PCs that appears in camera 2 of view 1
% Image2: homogeneous image coordinates (aka 3D bearing vectors) at view 2
%   size is 3*6
%   column 1-3: PCs that appears in camera 1 of view 2
%   column 4-6: PCs that appears in camera 2 of view 2
% R_camera: extrinsic rotation of perspective cameras in the two-camera rig 
%    size is 3*3*2
%    R_camera(:,:,i): extrinsic roation of camera i
% T_camera: extrinsic translation of perspective cameras in the two-camera rig 
%    size is 3*2
%    T_camera(:,i): extrinsic roation of camera i
% OUTPUT
% q_sols: real solutions for rotation using Cayley
%     size is 3*N, where N is the number of real solutions
% t_sols: real solutions for translation
%     size is 3*N, where N is the number of real solutions
% R_sols: real solutions for rotation using rotation matrix
%     size is 3*3*N, where N is the number of real solutions
% q_sols_all: complex solutions for rotation using Cayley
%     size is 3*48
% A: coefficient matrix (for debug only)

%% load data and format conversion
clear;
load('data_intra_pc1.mat');
Image1 = Image1(1:2,:);
Image2 = Image2(1:2,:);

Image1 = Image1(:, [1,3,5,2,4,6]);
Image2 = Image2(:, [1,3,5,2,4,6]);

Image1(3, :) = 1;
Image2(3, :) = 1;

R_camera = R_camera(:, :, 1:2);
T_camera = T_camera(:, 1:2);

%% ground truth
q = rotm2quat(Rf1tof2);
cay_gt = q(2:4)/q(1);
cay_gt = cay_gt(:);
t_gt = Tf1tof2;

%% run solver
%% 48 solutions
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_intra_6pc(Image1, Image2, R_camera, T_camera);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt
