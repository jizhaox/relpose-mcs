function [Image1, Image2, At, R_extrinsic_para, T_extrinsic_para, R_gt, cay_gt, t_gt] = generate_2AC_data(match_type)

[match_info, n_cam] = match_type_gcam_ac(match_type);
n_point = 2;

%% define random extrinsic parameters
cam_body_rotation = cell(n_cam, 1);
cam_body_offset = cell(n_cam, 1);
R_cam = cell(n_cam, 1);
t_cam = cell(n_cam, 1);
T_body_cam = cell(n_cam, 1);
for ii = 1:n_cam
    cay = rand(3, 1);
    cam_body_rotation{ii} = cayley_rotation(cay);
    cam_body_offset{ii} = rand(3, 1);
    
    R_cam{ii} = cam_body_rotation{ii}';
    t_cam{ii} = -R_cam{ii}*cam_body_offset{ii};
    % transformation from body reference to perspective camera references
    T_body_cam{ii} = [R_cam{ii} t_cam{ii}; 0 0 0 1];
end

%% define relative pose
cay = rand(3, 1);
R_gt = cayley_rotation(cay);
q = rotm2quat(R_gt);
cay_gt = q(2:4)/q(1);
cay_gt = cay_gt(:);

t_gt = rand(3, 1);

% transformation from body reference at time i to time j
T_gt = [R_gt t_gt; 0 0 0 1];

%% generating affine correspondences
% generating random scene points
[PT, Distance, Nvector] = generate_3Dscenepoints(n_point);
points_all = cell(n_point, 1);
for ii = 1:n_point
    points_all{ii} = struct('point', PT(:,:,ii));
end

%% extract point observations
% images at time i
x_i = cell(n_point, n_cam);
for ii = 1:n_point
    PT = points_all{ii}.point;
    for jj = 1:n_cam
%         tmp = R_cam{jj}*PT+t_cam{jj};
%         x_i{ii,jj} = tmp./tmp(3,:);
        tmp = R_cam{jj}*PT;
        tmp2 = tmp + repmat(t_cam{jj}(:), [1, size(tmp,2)]);
        x_i{ii,jj} = tmp2./ repmat(tmp2(3,:), [size(tmp2,1), 1]);
    end
end
% images at time j
Rc_j = cell(n_cam, 1);
tc_j = cell(n_cam, 1);
for ii = 1:n_cam
    tmp = T_body_cam{ii}*T_gt;
    Rc_j{ii} = tmp(1:3,1:3);
    tc_j{ii} = tmp(1:3,4);
end
x_j = cell(n_point, n_cam);
for ii = 1:n_point
    PT = points_all{ii}.point;
    for jj = 1:n_cam
%         tmp = Rc_j{jj}*PT+tc_j{jj};
%         x_j{ii,jj} = tmp./tmp(3,:);
        tmp = Rc_j{jj}*PT;
        tmp2 = tmp + repmat(tc_j{jj}, [1, size(tmp,2)]);
        x_j{ii,jj} = tmp2./ repmat(tmp2(3,:), [size(tmp2,1), 1]);
    end
end

%% construct observations
R_extrinsic_para = zeros(3,3,n_cam);
T_extrinsic_para = zeros(3, n_cam);
for ii = 1:n_cam
    R_extrinsic_para(:,:,ii) = cam_body_rotation{ii};
    T_extrinsic_para(:,ii) = cam_body_offset{ii};
end

Image1 = zeros(3, 2);
Image2 = zeros(3, 2);
At = zeros(2, 2, 2);

% compute an AC using the frist plane
Image1(:,1) = x_i{1,match_info{1}.idx1}(:,1);
Image2(:,1) = x_j{1,match_info{1}.idx2}(:,1);
H_c1i_c2j = DLT_homography(x_i{1,match_info{1}.idx1}(:,2:end)',x_j{1,match_info{1}.idx2}(:,2:end)');
At(:,:,1) = get_affinetransformation(H_c1i_c2j, Image1(:,1), Image2(:,1));
% compute the other AC using the second plane    
Image1(:,2) = x_i{2,match_info{2}.idx1}(:,1);
Image2(:,2) = x_j{2,match_info{2}.idx2}(:,1);
H_c2i_c1j = DLT_homography(x_i{2,match_info{2}.idx1}(:,2:end)',x_j{2,match_info{2}.idx2}(:,2:end)');
At(:,:,2) = get_affinetransformation(H_c2i_c1j, Image1(:,2), Image2(:,2));

%% check data
% For noise-free data, the residual of epipolar constraint and affine transformation constraints should be small
err_epipolar = zeros(2, 1);
err_affinetransformation = zeros(2, 2);
Tf_gt = [R_gt t_gt; 0 0 0 1];
Tf_cam = cell(n_cam, 1);
for ii = 1:n_cam
    Tf_cam{ii} = [R_extrinsic_para(:,:,ii), T_extrinsic_para(:,ii); 0, 0, 0, 1];
end
for ii = 1:2
    idx1 = match_info{ii}.idx1;
    idx2 = match_info{ii}.idx2;
    
    x1 = Image1(:, ii);
    x2 = Image2(:, ii);
    % relative pose between time i and time j 
    Hij = Tf_cam{idx2}\Tf_gt*(Tf_cam{idx1});
    Rij = Hij(1:3,1:3);
    tij = Hij(1:3,4);
    % epipolar constraint of PC
    x = tij;
    skew_tij=[0 -x(3) x(2) ; x(3) 0 -x(1) ; -x(2) x(1) 0 ];
    E =skew_tij*Rij;
    err_epipolar(ii) = x2'*E*x1;
    % affine transformation constraints
    At_33(1:2,1:2) = At(:,:,ii);
    At_33(3,3) = 1;
    equationerror = E'*x2 + At_33'*E*x1;
    err_affinetransformation(:,ii) = equationerror(1:2,1);
end
disp(['The maximum residual of epipolar constraint: ' num2str(max(abs(err_epipolar(:))))])
disp(['The maximum residual of affine transformation constraints: ' num2str(max(abs(err_affinetransformation(:))))])   
