function [R_extrinsic_para2, T_extrinsic_para2] = generic_interface_ac(R_extrinsic_para, T_extrinsic_para, match_type)

%% unified interface for cases 1~4

[match_info, n_cam] = match_type_gcam_ac(match_type);
if 1%strcmp(match_type, 'case1') || strcmp(match_type, 'case2') || strcmp(match_type, 'case3') || strcmp(match_type, 'case4')
    R_extrinsic_para2 = zeros(3,3,4);
    T_extrinsic_para2 = zeros(3, 4);
    for ii = 1:2
        idx1 = match_info{ii}.idx1;
        idx2 = match_info{ii}.idx2;
        R_extrinsic_para2(:,:,ii*2-1) = R_extrinsic_para(:,:,idx1);
        R_extrinsic_para2(:,:,ii*2) = R_extrinsic_para(:,:,idx2);
        T_extrinsic_para2(:,ii*2-1) = T_extrinsic_para(:,idx1);
        T_extrinsic_para2(:,ii*2) = T_extrinsic_para(:,idx2);
    end
end