%% test solver for intra-camera ACs

%% Interface of the solver
% [q_sols, t_sols, R_sols, q_sols_all, A] = solver_trans_intra_2ac(Image1, Image2, Ac, R_camera, T_camera);
% INPUT
% Image1: homogeneous image coordinates (aka 3D bearing vectors) at view 1
%   size is 3*2
%   column i: image coordinate of i-th AC at view 1
% Image2: homogeneous image coordinates (aka 3D bearing vectors) at view 2
%   size is 3*2
%   column i: image coordinate of i-th AC at view 2
% Ac: affine transformations
%    size is 2*2*2
%    Ac(:,:,i): affine transformation for i-th AC
% R_camera: extrinsic rotation of perspective cameras in the two-camera rig 
%    size is 3*3*2
%    R_camera(:,:,i): extrinsic roation of camera i
% T_camera: extrinsic translation of perspective cameras in the two-camera rig 
%    size is 3*2
%    T_camera(:,i): extrinsic roation of camera i
% OUTPUT
% q_sols: real solutions for rotation using Cayley
%     size is 3*N, where N is the number of real solutions
% t_sols: real solutions for translation
%     size is 3*N, where N is the number of real solutions
% R_sols: real solutions for rotation using rotation matrix
%     size is 3*3*N, where N is the number of real solutions
% q_sols_all: complex solutions for rotation using Cayley
%     size is 3*48
% A: coefficient matrix (for debug only)

%% load data and format conversion
clear;
load('data_intra_ac1.mat');
Image1 = Image1(1:2,:);
Image2 = Image2(1:2,:);
Ac = Ac(1:2,1:2,:);

Image1(3,:) = 1;
Image2(3,:) = 1;

%% ground truth
q = rotm2quat(Rf1tof2);
cay_gt = q(2:4)/q(1);
cay_gt = cay_gt(:);
t_gt = Tf1tof2;

%% run solver
%% 48 solutions
[cay_sols, t_sols, R_sols, cay_sols_all] = solver_trans_intra_2ac(Image1, Image2, Ac, R_camera, T_camera);
cay_sol = find_solution(cay_sols, cay_gt);
t_sol = find_solution(t_sols, t_gt);
cay_sol, t_sol, cay_gt, t_gt
